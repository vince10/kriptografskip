#!/usr/bin/env python3
def fast_exp(m, e, n):
    r = 1
    if 1 & e:
        r = m
    while e:
        e >>= 1
        print("c: ",e)
        m = (m * m) % n
        print("f: ",m)
        if e & 1:
            print("b-i: ", 1)
            r = (r * m) % n
            print("r: ",r)
        else:
            print("b-i: ",0)
    return r

def fast_exp_custom(m,e,n):
    c=0
    f=1
    binary = bin(e)[2:]
    i = len(binary)-1
    while i>=0:
        c = 2*c
        f = (f*f)%n
        index = len(binary)-1-i
        if binary[index]=="1":
            c+=1
            f = (f*m)% n
        print("b-"+str(i)+":",binary[index])
        print("c-" + str(i) + ":", c)
        print("f-" + str(i) + ":", f)
        i-=1
    return f

def main():
    m = int(input("Enter m:"))
    e = int(input("Enter e:"))
    n = int(input("Enter n:"))
    r = fast_exp_custom(m, e, n)
    print("{} ^ {} ≡ {} (mod {})".format(m, e, r, n))

if __name__ == '__main__':
    main()


