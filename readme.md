Python implementation of the index calculus method for solving discrete logarithms.

Prequisites

Install Python3 (any version)

Install sagemath

 ```
sudo apt-get install sagemath 
```

Usage

````
$ python3 py3-pyDLP.py

p: 18443, g: 37, h: 211, B: 5
searching for congruences.
congruences: 6
bases: 3
converting to matrix format.
solving linear system with sage:
value M: [[3, 7, 0], [1, 4, 0], [11, 0, 1], [2, 0, 4], [0, 2, 0], [2, 0, 2]]
value b: [16797, 13407, 14014, 18132, 13058, 5578]
exponents:  (5733, 15750, 6277)
sage done.
checking congruences:
Passed!

checking dlog exponents:
37^5733 = 2 (mod 18443)
37^15750 = 3 (mod 18443)
37^6277 = 5 (mod 18443)
Passed!

searching for k such that h*g^-k is B-smooth.
found k = 76
Solving the main dlog problem:

37^8500 = 211 (mod 18443) holds!
DLP solution: 8500

````
For concept background and etc, please refer to Jeffrey Hoffstein, Jill Pipher, J.H. Silverman - Introduction to Mathematical Cryptography-Springer (2010) book page 175-176 

